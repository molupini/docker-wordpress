import fire
import json
import boto3


class HttpFetch(object):
    
    def secret(self, id, key='null'):
        try:
            client = boto3.client('secretsmanager')
            response = client.get_secret_value(
                SecretId=f'sec-{id}'
            )
        except Exception as er:
            return 1
        # debugging
        j = json.loads(response["SecretString"])
        if key != "null":
            try:
                k = j[key]
                return k
            except KeyError as er:
                return 1
        else:
            return j

if __name__ == '__main__':
    fire.Fire(HttpFetch)