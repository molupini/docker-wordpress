# # base
# https://hub.docker.com/_/wordpress
FROM wordpress:latest as base 

# ENV PORT=80
# EXPOSE ${PORT}
# WORKDIR /app

ARG DB_HOST
ARG DB_PORT
ARG DB_USER
ARG DB_PASSWORD
ARG DB_NAME

ENV WORDPRESS_DB_HOST=${DB_HOST}:${DB_PORT}
ENV WORDPRESS_DB_USER=${DB_USER}
ENV WORDPRESS_DB_PASSWORD=${DB_PASSWORD}
ENV WORDPRESS_DB_NAME=${DB_NAME}

# debugging
# RUN echo ${WORDPRESS_DB_HOST}
# RECOMMENDED IF CONTROLLED SHUTDOWNS UNMANAGED 
# RUN apk add --no-cache tini 
# ENTRYPOINT [ "/sbin/tini", "--" ]

# # # development
FROM base as dev

# # # source 
FROM base as source

# # # audit
FROM base as audit

# # # production
FROM source as prod
