# WordPress

powered by,
    - docker, https://www.docker.co.za
    - word press, https://www.wordpress.co.za


### Prep

Open your favorite Terminal and run these commands.

First, if necessary:
```sh
$ mkdir ./[dir]
```
Second:
```sh
$ git init
```
Third:
```sh
$ git clone git@gitlab.com:molupini/docker-wordpress.git
```

### Build/Push

Easily done in a Docker container.
Make required changes within Dockerfile + compose files if necessary. When ready, simply use docker-compose to build your environment.
This will create the *docker-wordpress, ...* services with necessary dependencies.
Once done, simply run the following commends:

For dev, docker compose:
```sh
$ docker-compose build
$ docker-compose up
```

For prod, docker build, push
*app:release or version or stage within file*
*best to update your awscli before we start*
```sh
$ sudo apt-get upgrade awscli
$ cd [dir... Dockerfile] 
$ aws configure
$ `aws ecr get-login`
$ docker build -t [repo/app:release] .
$ docker tag [repo/app:release] [repo-url/repo/app:release]
$ docker push [repo-url/repo/app:release]
```

Example, 
Reference, https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_AWSCLI.html
```sh
# verify container path, might be sub directory 
# aws configure
$(aws ecr get-login --no-include-email --region eu-west-1)
# BUILD
docker build -t ecr-ecs-sgti-st-test-000016/app:prod \
--build-arg DB_HOST=rds-sgti-st-000007.cdftrf29c82k.eu-west-1.rds.amazonaws.com \
--build-arg DB_PORT=3306 \
--build-arg DB_USER=uprightlamb \
--build-arg DB_PASSWORD=M4TDDDmDsuhbLtMgxA \
--build-arg DB_NAME=TEST .

# TAG
docker tag ecr-ecs-sgti-st-test-000016/app:prod \
777120396959.dkr.ecr.eu-west-1.amazonaws.com/ecr-ecs-sgti-st-test-000016/app:prod

# PUSH
docker push 777120396959.dkr.ecr.eu-west-1.amazonaws.com/ecr-ecs-sgti-st-test-000016/app:prod
```

Example with helper.py powered by Boto3, 
```sh
# verify container path, might be sub directory 
# aws configure
$(aws ecr get-login --no-include-email --region eu-west-1)
# BUILD
docker build -t `python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecr_repo`:`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecs_version` \
--build-arg DB_HOST=`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b rds_address` \
--build-arg DB_PORT=`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b rds_port` \
--build-arg DB_USER=`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b usr` \
--build-arg DB_PASSWORD=`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b pass` \
--build-arg DB_NAME=`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b rds_db` .

# TAG
docker tag `python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecr_repo`:`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecs_version` \
`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecr_uri`:`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecs_version`

# PUSH
docker push `python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecr_uri`:`python3 ./util/helper.py secret 5e14457a1922ca0012f7d38b ecs_version`
```

*See Reference, https://docs.aws.amazon.com/AmazonECR/latest/userguide/ECR_AWSCLI.html*
